
security 最核心的东西就是  securityConfig

访问自带的登录接口：localhost:8080/login

访问自带的登出接口：localhost:8080/logout

访问重写的修改密码接口：localhost:8080/password  这个修改密码的功能需要自己手动实现

设置某个接口需要什么角色的权限   这个需要再securityConfig中设置

remember-me  这个功能的核心是什么  一定要理解清楚  不是简单的在框中 记住输过的账号和密码  而是关闭浏览器再打开浏览器访问项目中的资源  不需要重新登陆

权限管理策略
Spring Security 中提供的权限管理策略主要有两种类型:

基于过滤器的权限管理 (FilterSecurityInterceptor)
基于过滤器的权限管理主要是用来拦截 HTTP 请求，拦截下来之后，根据 HTTP 请求地址进行权限校验。
对比原理：拦截到这次的请求 url，根据 url 我们从数据库中取出能访问对应 url 的所有角色或权限信息，然后由 AccessDecisionManager 判断我们的登录用户是否有这些角色/权限中的一个

基于 AOP 的权限管理 (MethodSecurityInterceptor)
基于 AOP 权限管理主要是用来处理方法级别的权限问题。当需要调用某一个方法时，通过 AOP 将操作拦截下来，然后判断用户是否具备相关的权限。
对比原理：由 AOP 拦截下来添加了对应注解的方法的请求信息，然后获取当前登录用户的所有权限信息，判断我们的用户所有权限中是否有在注解中写死的权限，有的话就允许访问

原文链接：https://blog.csdn.net/miserable_world/article/details/132301854