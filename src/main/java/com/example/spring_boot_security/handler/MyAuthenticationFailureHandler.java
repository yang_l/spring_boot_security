package com.example.spring_boot_security.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author YangLi
 * @Date 2023/11/28 16:52
 * @注释
 */
@Slf4j
public class MyAuthenticationFailureHandler implements AuthenticationFailureHandler {

    // 这个就是在登录的时候 抛出的异常 会被这个方法拦截，然后在这里进行异常处理
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException {
        // 如果是验证码错误  则在头部信息中放入验证码错误的信息
        // 将错误信息作为请求属性传递  因为使用的是重定向  浏览器发新的请求  所以只能将信息放在session中
        exception.printStackTrace();
        request.getSession().setAttribute("errorMessage", exception.getMessage());

        // 重定向到请求到我自己的登录页面  重定向是浏览器收到服务器返回的重定向请求然后重新发起调用
        response.sendRedirect("/index");

        // 而下面是转发，这个是服务器内部完成的，客户端感知不到，转发时由于是在服务器内部完成的，所以转发的请求所携带的
        // 请求头，body，请求方式原请求一致
        //request.getRequestDispatcher("/index").forward(request, response);
    }
}
