package com.example.spring_boot_security.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author YangLi
 * @Date 2023/11/29 18:23
 * @注释
 */
public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
//        Map<String, Object> result = new HashMap<>();
//        result.put("msg", "登录成功");
//        result.put("status", 200);
//        result.put("authentication", authentication);
//        response.setContentType("application/json;charset=UTF-8");
//        //  下面这个方法是将对象转为json字符串的意思
//        String s = new ObjectMapper().writeValueAsString(result);
//        response.getWriter().println(s);
        response.sendRedirect("/home");

    }
}
