package com.example.spring_boot_security.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.util.*;

/**
 * @Author YangLi
 * @Date 2023/11/30 13:43
 * @注释
 */
@TableName("user")
@Data
public class User implements UserDetails {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String username;

    private String password;

    /**
     * 账户是否激活
     */
    private Boolean enabled;

    /**
     * 账户未过期
     */
    private Boolean accountNonExpired;

    /**
     * 账户未锁定
     */
    private Boolean accountNonLocked;

    /**
     * 密码未过期
     */
    private Boolean credentialsNonExpired;

    /**
     * 手机号
     */
    @Pattern(regexp = "^[0-9]{10}$", message = "Invalid phone number")
    private String phoneNum;

    /**
     * 邮箱
     */
    @Email(message = "Invalid email format")
    private String email;

    // 主要是用来更新密码  接收前端参数
    @TableField(exist = false)
    private String newPassword;

    //关系属性 用来存储当前用户所有角色信息
    @TableField(exist = false)
    private List<Role> roles = new ArrayList<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        roles.forEach(role -> {
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(role.getName());
            authorities.add(simpleGrantedAuthority);
        });
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
