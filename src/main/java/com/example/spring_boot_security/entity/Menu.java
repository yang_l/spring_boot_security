package com.example.spring_boot_security.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/12/18 10:53
 * @注释
 */
@Data
@TableName("menu")
public class Menu {

    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("pattern")
    private String pattern;

    @TableField(exist = false)
    private List<Role> roles;

}
