package com.example.spring_boot_security.fileter;

import com.example.spring_boot_security.smslogin.SmsAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author YangLi
 * @Date 2024/9/12 17:12
 * @注释
 */
public class SmsAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    // 这里这个配置表示  只拦截sms-login接口   这样其他的登录方式就不需要验证验证码了
    public SmsAuthenticationFilter(AuthenticationManager authenticationManager) {
        super("/sms-login");  // 短信登录的请求路径
        setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String phoneNumber = request.getParameter("phoneNumber");
        String code = request.getParameter("smsCode");

        SmsAuthenticationToken authRequest = new SmsAuthenticationToken(phoneNumber, code);

        // 终止过过滤器链
        return getAuthenticationManager().authenticate(authRequest);
    }
}





















