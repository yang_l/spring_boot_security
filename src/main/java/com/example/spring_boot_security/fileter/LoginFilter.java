package com.example.spring_boot_security.fileter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import com.fasterxml.jackson.core.type.TypeReference;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * @Author YangLi
 * @Date 2023/11/30 18:18
 * @注释 自定义前后端分离认证 Filter  (为了实现remember-me这个功能 从body中读取参数)   测试通过之后 就换回了自带的
 */


// 不要被名字误解了    这个和登录关系不大   就是为了实现remember-me这个功能  重要的事情说3次
// 不要被名字误解了    这个和登录关系不大   就是为了实现remember-me这个功能  重要的事情说3次
// 不要被名字误解了    这个和登录关系不大   就是为了实现remember-me这个功能  重要的事情说3次
public class LoginFilter extends UsernamePasswordAuthenticationFilter {

    public LoginFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    /**
     * 重写这个方法  可以让记住我这个功能走这个下面这个逻辑   就是自定义remember-me   不重写也行  security会使用自己默认的rememberMe逻辑
     * 所以为什么要写这个逻辑呢  是因为security 自带的rememberMe功能  只能是post请求  并且参数在request param中  如果参数在body中就需要重写
     *
     * @param request  ·
     * @param response ·
     * @return ·
     * @throws AuthenticationException ·
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        System.out.println("========================================");
        //1.判断是否是 post 方式请求
        if (!request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }
        //2.判断是否是 json 格式请求类型
        if (request.getContentType().equalsIgnoreCase(MediaType.APPLICATION_JSON_VALUE)) {
            //3.从 json 数据中获取用户输入用户名和密码进行认证 {"uname":"xxx","password":"xxx","remember-me":true}
            try {
                // 从请求参数中读取数据  并转成map
                Map<String, String> userInfo = new ObjectMapper().readValue(request.getInputStream(), new TypeReference<Map<String, String>>() {
                });
                String username = userInfo.get(getUsernameParameter());
                String password = userInfo.get(getPasswordParameter());
                String rememberValue = userInfo.get(AbstractRememberMeServices.DEFAULT_PARAMETER);
                if (!ObjectUtils.isEmpty(rememberValue)) {
                    request.setAttribute(AbstractRememberMeServices.DEFAULT_PARAMETER, rememberValue);
                }
                System.out.println("用户名: " + username + " 密码: " + password + " 是否记住我: " + rememberValue);
                UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
                setDetails(request, authRequest);
                return this.getAuthenticationManager().authenticate(authRequest);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return super.attemptAuthentication(request, response);
    }


}
