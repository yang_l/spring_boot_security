package com.example.spring_boot_security.fileter;

import com.example.spring_boot_security.exception.MyAuthenticationServiceException;
import com.mysql.cj.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class CaptchaAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    // 这里这个配置表示  只拦截login接口   这样oauth2的登录就不需要验证验证码了
    public CaptchaAuthenticationFilter(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
        // 设置过滤器处理的 URL 路径
        setFilterProcessesUrl("/login");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        // 这里需要注意的是 我使用的是表单提交 参数在x-www-form-urlencoded中，如果使用post请求参数在body中是否可以获取到  我没有测试过
        String captcha = request.getParameter("captcha");
        if (StringUtils.isNullOrEmpty(captcha)) {
            throw new MyAuthenticationServiceException("请输入验证码!");
        }
        // 从redis中获取验证码
        String redisCaptcha = redisTemplate.opsForValue().get("captcha");
        if (StringUtils.isNullOrEmpty(redisCaptcha)) {
            throw new MyAuthenticationServiceException("验证码已过期，请刷新!");
        }
        if (!captcha.equalsIgnoreCase(redisCaptcha)) {
            throw new MyAuthenticationServiceException("验证码错误!");
        }
        // 继续验证用户名和密码
        return super.attemptAuthentication(request, response);
    }
}
