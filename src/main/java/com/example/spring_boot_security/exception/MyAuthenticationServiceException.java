package com.example.spring_boot_security.exception;

import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * @Author YangLi
 * @Date 2024/9/10 17:07
 * @注释
 */
public class MyAuthenticationServiceException extends AuthenticationServiceException {

    public MyAuthenticationServiceException(String msg) {
        super(msg);
    }

    public MyAuthenticationServiceException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
