package com.example.spring_boot_security.config;

import com.example.spring_boot_security.fileter.CaptchaAuthenticationFilter;
import com.example.spring_boot_security.fileter.SmsAuthenticationFilter;
import com.example.spring_boot_security.handler.*;
import com.example.spring_boot_security.intercepter.CustomSecurityMetadataSource;
import com.example.spring_boot_security.service.UsernamePasswordUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.UrlAuthorizationConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @Author YangLi
 * @Date 2023/11/29 18:14
 * @注释
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig {

    private static final String[] ALLOW_EVERYONE_ACCESS = {"/vc.png", "/password", "/fileUpload", "/verificationCode",
            "/captcha", "/login", "/index", "/static/**", "/images/**", "/clear-error-message", "/oauth2/github-qrCode",
            "/oauth2/weChat-qrCode", "/register", "/oauth2/qq-qrCode", "sms-login", "/send-sms-code"};

    private static final String[] ALLOW_ADMIN_ACCESS = {"/rememberMeTest"};

    @Resource
    private UsernamePasswordUserService usernamePasswordUserService;

    @Resource
    private CustomSecurityMetadataSource customSecurityMetadataSource;

    // 这里配置了authenticationManager
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }

    // 我们在上面通过@Bean的方式初始化了这个东西  如果需要在这个类中使用 需要注入
    @Resource
    private AuthenticationManager authenticationManager;

    // 这里是验证码的拦截器  我们在登录的时候先验证验证码是否正确  正确的话再继续验证登录名和密码
    @Bean
    public CaptchaAuthenticationFilter captchaAuthenticationFilter() {
        CaptchaAuthenticationFilter filter = new CaptchaAuthenticationFilter(authenticationManager);
        //这里一定要注意  一定要指定处理器 必须是security自己的或者继承了它的，不能直接在自己的过滤器中抛出就不管了，这样无法被处理  即使使用了全局异常也不行 捕获不了
        // **********************(这个东西很重要，很重要，很重要)
        // 这里配置登录成功的处理器
        filter.setAuthenticationSuccessHandler(new MyAuthenticationSuccessHandler());
        // 这里配置 登录失败的处理器
        filter.setAuthenticationFailureHandler(new MyAuthenticationFailureHandler());
        return filter;
    }

    // 这里和上面配置图形验证码的方式几乎一样
    @Bean
    public SmsAuthenticationFilter smsAuthenticationFilter() {
        SmsAuthenticationFilter filter = new SmsAuthenticationFilter(authenticationManager);
        filter.setAuthenticationSuccessHandler(new MyAuthenticationSuccessHandler());
        // 这里配置 登录失败的处理器
        filter.setAuthenticationFailureHandler(new MyAuthenticationFailureHandler());
        return filter;
    }

    // 配置这个是为了替换security默认的处理方式   默认处理方式请求参数在requestParam中  我自己实现的是从body中读取数据
    // 写这个是为了了解rememberMe的实现原理  为了适应更多的请求方式
    // @Bean
    // public LoginFilter loginFilter() {
    //     LoginFilter loginFilter = new LoginFilter(authenticationManager);
    //     loginFilter.setRememberMeServices(rememberMeServices());
    //     loginFilter.setAuthenticationSuccessHandler(new MyAuthenticationSuccessHandler());
    //     loginFilter.setAuthenticationFailureHandler(new MyAuthenticationFailureHandler());
    //     return loginFilter;
    // }

    // 使用自定义的rememberMeService   不用自己的  可以直接 new PersistentTokenBasedRememberMeServices
    // 这个 bean  也是为了实现  remember-me 而配置的   配置的是自己的实现类  默认的话 使用下面那个
    // @Bean
    // public RememberMeServices rememberMeServices() {
    //     return new MyPersistentTokenBasedRememberMeServicesImpl(UUID.randomUUID().toString(), userService, new InMemoryTokenRepositoryImpl());
    // }


    // 这里返回的就是security自带的 RememberMeServices 实现类了
    @Bean
    public RememberMeServices rememberMeServices() {
        return new PersistentTokenBasedRememberMeServices(UUID.randomUUID().toString(), usernamePasswordUserService, new InMemoryTokenRepositoryImpl());
    }


    @Bean
    public SecurityFilterChain securityFilterChain(@Autowired HttpSecurity http) throws Exception {
        ApplicationContext applicationContext = http.getSharedObject(ApplicationContext.class);
        // 这里是在程序启动时就给定可以访问的路径的权限配置
        http.authorizeHttpRequests(authorizeRequests ->
                {
                    try {
                        authorizeRequests
                                // .antMatchers(ALLOW_ADMIN_ACCESS)  后面可以使用下面这些方法
                                // hasAuthority(String authority)	当前用户是否具备指定权限
                                //hasAnyAuthority(String… authorities)	当前用户是否具备指定权限中任意一个
                                //hasRole(String role)	当前用户是否具备指定角色
                                //hasAnyRole(String… roles);	当前用户是否具备指定角色中任意一个
                                //permitAll();	放行所有请求/调用
                                //denyAll();	拒绝所有请求/调用
                                //isAnonymous();	当前用户是否是一个匿名用户
                                //isAuthenticated();	当前用户是否已经认证成功
                                //isRememberMe();	当前用户是否通过 Remember-Me 自动登录
                                //isFullyAuthenticated();	当前用户是否既不是匿名用户又不是通过 Remember-Me 自动登录的
                                //hasPermission(Object targetId, Object permission);	当前用户是否具备指定目标的指定权限信息
                                //hasPermission(Object targetId, String targetType, Object permission);	当前用户是否具备指定目标的指定权限信息

                                // 但是这样在代码中写死比较死板 每一个集合在项目启动的时候就已经固定了
                                // 我们能不能使用一个拦截器或者过滤器在请求访问的时候去数据库中获取该用户具有哪些权限呢？

                                // 表示 ALLOW_EVERYONE_ACCESS  这个集合中请求的路径 允许所有人访问。
                                .mvcMatchers(ALLOW_EVERYONE_ACCESS).permitAll()
                                // 表示 ALLOW_ADMIN_ACCESS  这个集合中请求的路径 只允许 admin 访问。  这里要区分大小写 数据库大写这里就需要大写  数据库小写这里就小写
                                .antMatchers(ALLOW_ADMIN_ACCESS).hasRole("ADMIN")
                                // 表示对于其他所有请求，用户必须经过身份验证（登录）。
                                .anyRequest().authenticated()
                                .and()
                                //  这里配置OAuth的登录
                                .oauth2Login();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        );
        // 下面这个 hrrp.apply() 这个方法的意思就是从数据获取配置的权限信息  这里就可以通过url来给定权限   可以在数据库中配置
        http.apply(new UrlAuthorizationConfigurer<>(applicationContext))
                .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
                    @Override
                    public <O extends FilterSecurityInterceptor> O postProcess(O object) {
                        object.setSecurityMetadataSource(customSecurityMetadataSource);
                        // 这里设置false表示其他请求任何人都可以访问   这里设置true表示除了了数据库中配置的信息 其他请求任何人都不可以访问
                        object.setRejectPublicInvocations(false);
                        return object;
                    }
                });
        // 配置登录处理器
        http
                .formLogin(formLogin ->
                        formLogin
                                // 表单提交路径 就是登录页  这里配置的路径会默认放开权限  即使不加.permitAll() 也是一样的
                                .loginPage("/index").permitAll()
                                // 这个表示登录由哪个路径处理  这里配置的路径会默认放开权限  即使不加.permitAll() 也是一样的
                                .loginProcessingUrl("/login").permitAll()
                                // 登录成功后访问哪个接口   这里只有使用默认的处理器的时候才会生效
                                // 由于我在登录之前添加了验证码的过滤器 并继承了UsernamePasswordAuthenticationFilter 导致默认的处理器不会生效  所以这个地方配不配置都一样，反正不会生效
                                .defaultSuccessUrl("/home", true)
                                // 登录成功   这里只有使用默认的处理器的时候才会生效
                                // 由于我在登录之前添加了验证码的过滤器 并继承了UsernamePasswordAuthenticationFilter 导致默认的处理器不会生效  所以这个地方配不配置都一样，反正不会生效
                                // 具体的成功后的处理器是在本类的第 66 行配置
                                .successHandler(new MyAuthenticationSuccessHandler())
                                // 登录失败   这里只有使用默认的处理器的时候才会生效
                                // 由于我在登录之前添加了验证码的过滤器 并继承了UsernamePasswordAuthenticationFilter 导致默认的处理器不会生效  所以这个地方配不配置都一样，反正不会生效
                                // 具体的失败后处理器是在本类的第 68 行配置
                                .failureHandler(new MyAuthenticationFailureHandler())

                )
                // 配置OAuth2登录点也使用这个路径
                .oauth2Login(oauth2Login ->
                        oauth2Login
                                // 登录按键所在那个页面
                                .loginPage("/index")
                                // 成功后访问哪个接口
                                .defaultSuccessUrl("/home", true)
                )
                // 配置rememberMe  这里的逻辑就是 先对账号密码进行 rememberMe的逻辑处理  之后会去调用login  继续登录 返回的时候就带有rememberMe的东西了   一般是token
                .rememberMe(remember ->
                        remember
                                // 使用这个Service判断是否需要rememberMe  之后会加载登录逻辑
                                .rememberMeServices(rememberMeServices())
                                // 自定义key  这里的key就是一个密钥  生成和解析的时候会用到  防止不同的应用使用同一个配置，使用同一个token可以访问
                                // 具体点就是  A B两个应用   都使用登录，然后用户和密码都是admin 123456  那么这样A放回的tokenB也可以解析出来
                                // 如果加上key  那么解析的时候  就需要这个key相同才能解析
                                .key("remember-me")
                                // 过期时间  单位: s
                                .tokenValiditySeconds(10000)
                )
                // 配置logout处理器
                .logout(logout ->
                        logout
                                .logoutRequestMatcher(new OrRequestMatcher(
                                        // 这个配置表示用户访问 /logout1的时候 会触发登出操作  并且只处理http的请求方式
                                        new AntPathRequestMatcher("/logout1", "GET"),
                                        // 这个配置表示用户访问 /logout的时候 会触发登出操作  并且只处理http的请求方式
                                        new AntPathRequestMatcher("/logout", "GET")
                                ))
                                // 登出成功后执行自定义的成功处理逻辑。
                                .logoutSuccessHandler(new MyLogoutSuccessHandler())
                                .permitAll()
                )
                // 配置异常处理器
                .exceptionHandling(exceptionHandling ->
                        exceptionHandling
                                // 403 处理方式  授权异常
                                .accessDeniedHandler(new MyAccessDeniedHandler())
                                // 未登录的处理方式
                                .authenticationEntryPoint(new MyAuthenticationEntryPoint())
                )
                .userDetailsService(usernamePasswordUserService)
                // 配置会话管理
                .sessionManagement(sessionManagement ->
                                sessionManagement
                                        // 设置会话的并发数量
                                        .maximumSessions(1)
                                        // session过期处理策略
                                        .expiredSessionStrategy(new RepeatEntrySessionInformationExpiredStrategy())
                                        // 过期后访问跳转至login
                                        .expiredUrl("/index")
                        // 不配置下面这个 ： 当同一用户在其他地方登录  那么当前用户会被挤掉
                        // 配置下面这个： 当前用户登录成功之后  不允许再次登录  必须等用户下线才能再次登录
                        // .maxSessionsPreventsLogin(true)
                )
                .csrf().disable();

        // at: 用来某个 filter 替换过滤器链中哪个 filter
        // before: 放在过滤器链中哪个 filter 之前
        // after: 放在过滤器链中那个 filter 之后
        // 这里  是将自定义的remember-me过滤器替换掉自带的
        // http.addFilterAt(loginFilter(), UsernamePasswordAuthenticationFilter.class);
        // 在 UsernamePasswordAuthenticationFilter 过滤器之前  添加一个验证码的过滤器 验证码验证成功之后才继续验证username和password
        http.addFilterBefore(captchaAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
        // 这里再配置一个短信登录的拦截器
        http.addFilterBefore(smsAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }

    // 配置加密方式  需要在数据库存储密文
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
