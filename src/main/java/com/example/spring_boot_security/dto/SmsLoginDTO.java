package com.example.spring_boot_security.dto;

import lombok.Data;

/**
 * @Author YangLi
 * @Date 2024/9/11 18:19
 * @注释
 */
@Data
public class SmsLoginDTO {

    private String phoneNumber;

    private String verificationCode;
}
