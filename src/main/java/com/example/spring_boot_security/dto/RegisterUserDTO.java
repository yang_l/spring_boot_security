package com.example.spring_boot_security.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author YangLi
 * @Date 2023/12/4 17:12
 * @注释
 */
@Data
public class RegisterUserDTO {

    @TableId(value = "id", type = IdType.AUTO)
    private long id;

    @NotNull
    private String username;

    @NotNull
    private String password;

    //账户是否激活
    private Integer enabled;

    //账户未过期
    private Integer accountNonExpired;

    //账户是未锁定
    private Integer accountNonLocked;

    //密码未过期
    private Integer credentialsNonExpired;
}
