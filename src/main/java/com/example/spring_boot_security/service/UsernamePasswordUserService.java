package com.example.spring_boot_security.service;

import com.example.spring_boot_security.dto.RegisterUserDTO;
import com.example.spring_boot_security.global.ResultResponse;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Author YangLi
 * @Date 2023/11/30 13:51
 * @注释
 */

// 实现 UserDetailsService  这个接口是为了写登录逻辑   实现  UserDetailsPasswordService  是为了修改密码
public interface UsernamePasswordUserService extends UserDetailsService, UserDetailsPasswordService {

    ResultResponse<?> register(RegisterUserDTO registerUserDTO);

    ResultResponse<?> verificationCode(String phoneNum);
}
