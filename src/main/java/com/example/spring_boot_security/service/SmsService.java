package com.example.spring_boot_security.service;

/**
 * @Author YangLi
 * @Date 2024/9/11 18:21
 * @注释
 */
public interface SmsService {

    // 具体短信发送操作请到：https://blog.csdn.net/m0_71666771/article/details/142177586
    boolean sendVerificationCode(String phoneNumber, String code);

}
