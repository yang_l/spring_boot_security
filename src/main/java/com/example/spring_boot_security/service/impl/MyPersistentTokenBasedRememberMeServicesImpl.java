package com.example.spring_boot_security.service.impl;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author YangLi
 * @Date 2023/12/1 10:03
 * @注释 实现记住我功能
 */


// 不要被名字误解了    这个和登录关系不大   就是为了实现remember-me这个功能  重要的事情说3次  用这个类中方法判断是否开启了  remenber-me这个功能
// 不要被名字误解了    这个和登录关系不大   就是为了实现remember-me这个功能  重要的事情说3次  用这个类中方法判断是否开启了  remenber-me这个功能
// 不要被名字误解了    这个和登录关系不大   就是为了实现remember-me这个功能  重要的事情说3次  用这个类中方法判断是否开启了  remenber-me这个功能
public class MyPersistentTokenBasedRememberMeServicesImpl extends PersistentTokenBasedRememberMeServices {

    // 构造函数
    public MyPersistentTokenBasedRememberMeServicesImpl(String key, UserDetailsService userDetailsService, PersistentTokenRepository tokenRepository) {
        super(key, userDetailsService, tokenRepository);
    }

    /**
     * 自定义前后端分离获取 remember-me 方式
     *
     * @param parameter ..
     */
    @Override
    protected boolean rememberMeRequested(HttpServletRequest request, String parameter) {
        String paramValue = request.getAttribute(parameter).toString();
        if (paramValue != null) {
            if (paramValue.equalsIgnoreCase("true") || paramValue.equalsIgnoreCase("on")
                    || paramValue.equalsIgnoreCase("yes") || paramValue.equals("1")) {
                return true;
            }
        }
        return false;
    }
}
