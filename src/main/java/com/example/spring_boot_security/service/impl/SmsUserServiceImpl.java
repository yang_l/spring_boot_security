package com.example.spring_boot_security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.spring_boot_security.entity.User;
import com.example.spring_boot_security.mapper.RoleMapper;
import com.example.spring_boot_security.mapper.UserMapper;
import com.example.spring_boot_security.service.SmsUserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author YangLi
 * @Date 2024/9/12 18:19
 * @注释
 */
@Service
public class SmsUserServiceImpl implements SmsUserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private RoleMapper roleMapper;

    // 通过电话号码+短信验证码登录的情况需要 通过电话号码去查询用户，并将权限塞入
    @Override
    public UserDetails loadUserByUsername(String phoneNum) throws UsernameNotFoundException {
        LambdaQueryWrapper<User> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(StringUtils.isNotBlank(phoneNum), User::getPhoneNum, phoneNum);
        User user = userMapper.selectOne(wrapper);
        user.setRoles(roleMapper.getRolesByUid(user.getId()));
        return user;
    }
}
