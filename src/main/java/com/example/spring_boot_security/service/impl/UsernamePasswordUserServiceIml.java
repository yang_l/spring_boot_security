package com.example.spring_boot_security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.spring_boot_security.dto.RegisterUserDTO;
import com.example.spring_boot_security.entity.User;
import com.example.spring_boot_security.entity.UserRole;
import com.example.spring_boot_security.global.BizException;
import com.example.spring_boot_security.global.ExceptionEnum;
import com.example.spring_boot_security.global.ResultResponse;
import com.example.spring_boot_security.mapper.RoleMapper;
import com.example.spring_boot_security.mapper.UserMapper;
import com.example.spring_boot_security.mapper.UserRoleMapper;
import com.example.spring_boot_security.service.UsernamePasswordUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @Author YangLi
 * @Date 2023/11/30 13:51
 * @注释
 */
@Service
@Slf4j
public class UsernamePasswordUserServiceIml implements UsernamePasswordUserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    // 需要实现 UserDetailsService 这个接口   这里实现的UserService继承了这个接口
    // 判断是否满足登录条件  并将登录人的信息返回  校验密码  这个一步不需要我们自己验证  security框架会验证密码
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 使用postman直接访问需要权限的资源时，security回先登录，但是由于没有将cookie带上，所以解析不出username和password  会出现username为null的情况
        if (StringUtils.isBlank(username))
            throw new BizException(ExceptionEnum.LOGIN_FIRST);
        LambdaQueryWrapper<User> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(StringUtils.isNotBlank(username), User::getUsername, username);
        User user = userMapper.selectOne(wrapper);
        // security 框架会自己判断是否锁定，过时...  这里可以自定义
//        if (ObjectUtils.isEmpty(user))
//            throw  new BizException(ExceptionEnum.LOGIN_FAILURE);
//        if (!user.getEnabled())
//            throw  new BizException(ExceptionEnum.ACCOUNT_ENABLED);
//        if (!user.getAccountNonExpired())
//            throw  new BizException(ExceptionEnum.ACCOUNT_EXPIRED);
//        if (!user.getAccountNonLocked())
//            throw  new BizException(ExceptionEnum.ACCOUNT_LOCKED);
//        if (!user.getCredentialsNonExpired())
//            throw  new BizException(ExceptionEnum.CREDENTIALS_EXPIRED);
        user.setRoles(roleMapper.getRolesByUid(user.getId()));
        return user;
    }

    // 修改密码  需要实现 UserService 这个接口 这里实现的UserService继承了这个接口
    // 然后重写其中的 updatePassword 这个接口就可以按自己的想法修改密码了
    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
        User userFromMysql = userMapper.selectOne(new UpdateWrapper<User>().eq("username", user.getUsername()));
        if (ObjectUtils.isEmpty(userFromMysql))
            throw new BizException(ExceptionEnum.USER_NOT_EXIST);
        String encode = new BCryptPasswordEncoder().encode(newPassword);
        LambdaUpdateWrapper<User> wrapper = Wrappers.lambdaUpdate();
        wrapper.set(User::getPassword, encode);
        wrapper.eq(User::getUsername, user.getUsername());
        int result = userMapper.update(wrapper);
        if (result == 1) {
            ((User) user).setPassword(encode);
        }
        return user;
    }


    /**
     * 用户注册
     *
     * @param registerUserDTO ·
     * @return ·
     */
    @Override
    public ResultResponse<?> register(RegisterUserDTO registerUserDTO) {
        LambdaQueryWrapper<User> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(StringUtils.isNotBlank(registerUserDTO.getUsername()), User::getUsername, registerUserDTO.getUsername());
        User userInDataSource = userMapper.selectOne(wrapper);
        if (!ObjectUtils.isEmpty(userInDataSource))
            throw new BizException(ExceptionEnum.USERNAME_EXIST);
        String encode = new BCryptPasswordEncoder().encode(registerUserDTO.getPassword());
        RegisterUserDTO registerUser = new RegisterUserDTO();
        registerUser.setUsername(registerUserDTO.getUsername());
        registerUser.setPassword(encode);
        registerUser.setEnabled(1);
        registerUser.setAccountNonExpired(1);
        registerUser.setAccountNonLocked(1);
        registerUser.setCredentialsNonExpired(1);
        int insert = userMapper.insertUser(registerUser);
        if (insert == -1)
            return ResultResponse.error(ExceptionEnum.ACCOUNT_CREATE_FAILURE);
        UserRole userRole = new UserRole();
        userRole.setUid(registerUser.getId());
        userRole.setRid(3L);
        userRoleMapper.insert(userRole);
        return ResultResponse.success(ExceptionEnum.ACCOUNT_CREATE_SUCCESS);
    }

    @Override
    public ResultResponse<?> verificationCode(String phoneNumber) {
        // todo 给这个phoneNumber发送短信验证码  并将验证码放入redis 给定过期时间为3分钟
        // 这里使用随机数模拟发短信并存入redis中
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            stringBuilder.append(random.nextInt(9));
        }
        redisTemplate.opsForValue().set(phoneNumber, stringBuilder.toString(), 180, TimeUnit.SECONDS);
        System.out.println(redisTemplate.opsForValue().get(phoneNumber));
        log.info("验证码为：{} ， 3分钟内有效", stringBuilder.toString());
        // 这个就是当前登录的用户信息  是从session中获取的
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        String username = authentication.getName();
        return ResultResponse.success("发送成功");
    }
}

































