package com.example.spring_boot_security.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Author YangLi
 * @Date 2024/9/12 18:18
 * @注释
 */
public interface SmsUserService extends UserDetailsService {
}
