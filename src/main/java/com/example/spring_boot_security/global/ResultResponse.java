package com.example.spring_boot_security.global;

import com.alibaba.fastjson.JSON;
import lombok.Data;

/**
 * @description: 自定义数据传输
 * @author: YangLi
 * @date: 2023/7/20
 */
@Data
public class ResultResponse<T> {
    /**
     * 响应代码
     */
    private String code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 响应结果
     */
    private T result;

    private Boolean success;


    public ResultResponse() {
    }

    public ResultResponse(BaseErrorInfoInterface errorInfo) {
        this.code = errorInfo.getResultCode();
        this.message = errorInfo.getResultMsg();
    }

    /**
     * 成功
     *
     * @return .
     */
    public static <T> ResultResponse<T> success() {
        return success(null);
    }

    /**
     * 成功
     *
     * @param data .
     * @return .
     */
    public static <T> ResultResponse<T> success(T data) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(ExceptionEnum.SUCCESS.getResultCode());
        rb.setMessage(ExceptionEnum.SUCCESS.getResultMsg());
        rb.setResult(data);
        rb.setSuccess(true);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(BaseErrorInfoInterface errorInfo) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(errorInfo.getResultCode());
        rb.setMessage(errorInfo.getResultMsg());
        rb.setSuccess(false);
        rb.setResult(null);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(String code, String message) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(code);
        rb.setMessage(message);
        rb.setSuccess(false);
        rb.setResult(null);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(ExceptionEnum exceptionEnum) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(exceptionEnum.getResultCode());
        rb.setMessage(exceptionEnum.getResultMsg());
        rb.setResult(null);
        rb.setSuccess(false);
        return rb;
    }


    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(String code, String message, T result) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(code);
        rb.setMessage(message);
        rb.setResult(result);
        rb.setSuccess(false);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(ExceptionEnum exceptionEnum, T result) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode(exceptionEnum.getResultCode());
        rb.setMessage(exceptionEnum.getResultMsg());
        rb.setResult(result);
        rb.setSuccess(false);
        return rb;
    }

    /**
     * 失败
     */
    public static <T> ResultResponse<T> error(String message) {
        ResultResponse<T> rb = new ResultResponse<>();
        rb.setCode("500");
        rb.setMessage(message);
        rb.setSuccess(false);
        return rb;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}