package com.example.spring_boot_security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.spring_boot_security.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author YangLi
 * @Date 2023/12/4 16:18
 * @注释
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {
}
