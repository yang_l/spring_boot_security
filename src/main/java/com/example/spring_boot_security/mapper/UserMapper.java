package com.example.spring_boot_security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.spring_boot_security.dto.RegisterUserDTO;
import com.example.spring_boot_security.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author YangLi
 * @Date 2023/11/30 13:47
 * @注释
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    int insertUser(RegisterUserDTO registerUserDTO);
}
