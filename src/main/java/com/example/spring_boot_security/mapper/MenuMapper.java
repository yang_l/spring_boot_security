package com.example.spring_boot_security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.spring_boot_security.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/12/18 11:01
 * @注释
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> getAllMenu();

}
