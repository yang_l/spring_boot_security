package com.example.spring_boot_security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.spring_boot_security.entity.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2023/11/30 14:04
 * @注释
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    // 根据用户id查询角色信息
    List<Role> getRolesByUid(Long uid);
}
