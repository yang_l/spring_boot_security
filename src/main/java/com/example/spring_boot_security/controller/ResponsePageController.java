package com.example.spring_boot_security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author YangLi
 * @Date 2024/8/13 9:25
 * @注释
 */
@Controller
@RequestMapping
public class ResponsePageController {

    /**
     * 这里写这个请求的原因是  我们整合了Oauth2后  回调了地址处理完成之后  会访问  localhost:8080/   这里就是给一个调用成功后的页面展示
     * 下面这个接口就是返回一个展示页面   如果出现 404 就不再是常规后端的路径不存在的问题    而是有可能我们返回的视图找不到  没有放在指定的位置
     * 我这里使用的 them leaf index。html  应该放在 resource下 的templates 文件夹下面
     * 导入依赖   spring-boot-starter-thymeleaf
     * 配置yml
     * spring.mvc.view.prefix=/templates/
     * spring.mvc.view.suffix=.html
     *
     * @param model 这个参数是 web请求自带的不许我们手动传递   类似于  HttpServletRequest 和 HttpServletResponse
     * @return `
     */
    @GetMapping("/home")
    public String indexPage(Model model) {
        model.addAttribute("name", "主页");
        return "response";
    }

    @GetMapping("/index")
    public String login(Model model) {
        model.addAttribute("name", "登录页");
        return "index";
    }

    /**
     * 清除session中的会话信息 并重定向到/index 页面
     *
     * @return ·
     */
    @GetMapping("/clear-error-message")
    public String clearErrorMessage(HttpServletRequest request) {
        request.getSession().removeAttribute("errorMessage");
        return "redirect:/index";
    }
}
