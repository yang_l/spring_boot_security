package com.example.spring_boot_security.controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author YangLi
 * @Date 2023/12/18 18:09
 * @注释
 */
@RestController
@RequestMapping("oauth2")
public class Oauth2Controller {

    private static final int WIDTH = 250;

    private static final int HEIGHT = 250;

    @Value("${server:port}")
    private String port;

    @GetMapping("/hello")
    public DefaultOAuth2User hello() {
        System.out.println("hello");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (DefaultOAuth2User) authentication.getPrincipal();
    }

    @GetMapping("/github-qrCode")
    public void getQRCode(HttpServletResponse response) throws IOException, WriterException {
        // github 需要外网访问  手机微信扫码打不开页面
        String githubAuthUrl = "https://github.com/login/oauth/authorize?client_id=1440d31ede40f78f7059&redirect_uri=http://localhost:" + port + "/login/oauth2/code/github&scope=user"; // GitHub OAuth2 授权链接

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(githubAuthUrl, BarcodeFormat.QR_CODE, WIDTH, HEIGHT);

        response.setContentType("image/png");
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", response.getOutputStream());
    }

    @GetMapping("/weChat-qrCode")
    public void getWeChatQRCode(HttpServletResponse response) throws IOException, WriterException {
        String githubAuthUrl = "https://github.com/login/oauth/authorize?client_id=你的clientId&redirect_uri=你的重定向地址&scope=根据具体接入的app填写也可以不填"; //weChat OAuth2 授权链接

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(githubAuthUrl, BarcodeFormat.QR_CODE, WIDTH, HEIGHT);

        response.setContentType("image/png");
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", response.getOutputStream());
    }

    @GetMapping("/qq-qrCode")
    public void getQqQRCode(HttpServletResponse response) throws IOException, WriterException {
        String githubAuthUrl = "https://github.com/login/oauth/authorize?client_id=你的clientId&redirect_uri=你的重定向地址&scope=根据具体接入的app填写也可以不填"; //qq OAuth2 授权链接

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(githubAuthUrl, BarcodeFormat.QR_CODE, WIDTH, HEIGHT);

        response.setContentType("image/png");
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", response.getOutputStream());
    }
}
