package com.example.spring_boot_security.controller;

import com.example.spring_boot_security.service.SmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @Author YangLi
 * @Date 2024/9/11 18:17
 * @注释
 */
@RestController
@RequestMapping
@Slf4j
public class SmsController {

    @Resource
    private SmsService smsService;

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @PostMapping("/send-sms-code")
    public String sendCode(@RequestParam String phoneNumber) {
        Random random = new Random();
        // 生成一个范围在100000到999999之间的随机数   random.nextInt(900000): 生成一个900000以内的随机整数
        String code = String.valueOf(100000 + random.nextInt(900000));
        // 验证 验证码发送 是否发送成功 成功的话将验证码放入redis
        if (smsService.sendVerificationCode(phoneNumber, code)) {
            // 你可以将验证码存储在Redis中，并设置过期时间
            redisTemplate.opsForValue().set("SMS_" + phoneNumber, code, 50, TimeUnit.MINUTES);
            return "验证码发送成功";
        } else return "验证码发送失败";
    }
}
