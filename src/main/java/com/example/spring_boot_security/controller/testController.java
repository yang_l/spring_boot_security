package com.example.spring_boot_security.controller;

import com.example.spring_boot_security.dto.RegisterUserDTO;
import com.example.spring_boot_security.entity.User;
import com.example.spring_boot_security.global.ResultResponse;
import com.example.spring_boot_security.service.UsernamePasswordUserService;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


/**
 * @Author YangLi
 * @Date 2023/11/29 17:40
 * @注释
 */
@RestController
@RequestMapping
@Slf4j
public class testController {

    @Resource
    private UsernamePasswordUserService usernamePasswordUserService;

    @Resource
    private DefaultKaptcha captchaProducer;

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 修改密码  需要登录成功才能修改    还有一个是忘记密码  逻辑与这个不同   忘记密码时，不需要登录
     *
     * @param user user
     * @return user
     */
    @PostMapping("/password")
    public UserDetails updatePassword(@RequestBody User user) {
        return usernamePasswordUserService.updatePassword(user, user.getNewPassword());
    }

    /**
     * remember-me  这个并不是  输入框中记住我输入过的东西   这个东西 浏览器自带的记住密码 就能实现
     * 怎么理解这个东西   初学者一般认为 RememberMe 功能就是把用户名/密码用 Cookie 保存在浏览器中，下次登录时不用再次输入用户名/密码。这个理解显然是不对
     * <p>
     * 我们这里所说的 RememberMe 是一种服务器端的行为。传统的登录方式基于  Session 会话，一旦用户关闭浏览器重新打开，就要再次登录，这样太过于烦琐。
     * 如果能有一种机制，让用户关闭并重新打开浏览器之后，还能继续保持认证状态，就会方便很多，RememberMe 就是为了解决这一需求而
     * <p>
     * 这是自定义 remember-me 的测试  现在用的是 security 自带的
     * 如何测试   在cookie中清除remember-me  然后直接访问这个接口  看能不能访问  （应该是不能）
     * 然后登录 关闭浏览器  打开浏览器再访问这个接口  看能不能访问  （应该是能）
     * <p>
     * 这里需要注意的是   实现这个功能需要前端配合   将token持久化   重新打开浏览器的时候需要带上这个token  后端才能解析
     */
//    @Secured("ROLE_admin")  // 表示只有admin用户可以访问
//    @PreAuthorize("hasRole('ROLE_admin')")  // 表示在方法开始前先判断权限
    @GetMapping("/rememberMeTest")
    public String rememberMeTest() {
        return "this is a test result about remember me !";
    }

    /**
     * 注册账号 这里创建的是普通用户  记住放开权限（在securityConfig中配置）  不需要登录就可以访问
     *
     * @param registerUserDTO ·
     * @return ·
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/register")
    public ResultResponse<?> register(@RequestBody @Validated RegisterUserDTO registerUserDTO) {
        return usernamePasswordUserService.register(registerUserDTO);
    }

    /**
     * 获取验证码  模拟手机验证码
     *
     * @return ·
     */
    @PostMapping("/verificationCode")
    public ResultResponse<?> verificationCode(@RequestBody String phoneNumber) {
        return usernamePasswordUserService.verificationCode(phoneNumber);
    }

    /**
     * 这个是返回登录页面使用的验证码图片   这里只是返回一张图片哈   具体的验证码验证需要通过  CaptchaAuthenticationFilter  拦截器 或者过滤器去实现
     *
     * @param response ·
     * @throws IOException ·
     */
    @GetMapping("/captcha")
    public void getCaptcha(HttpServletResponse response) throws IOException {
        String captchaText = captchaProducer.createText();
        log.info("验证码是： {}", captchaText);
        // 将验证码放在redis中 过期时间为30s
        redisTemplate.opsForValue().set("captcha", captchaText, 30, TimeUnit.SECONDS);
        BufferedImage captchaImage = captchaProducer.createImage(captchaText);
        response.setContentType("image/jpeg");
        ImageIO.write(captchaImage, "jpg", response.getOutputStream());
    }

    // 下面几个方法的访问权限就放在数据库中
    @GetMapping("/admin/hello")
    public String admin() {
        return "hello admin";
    }

    @GetMapping("/user/hello")
    public String user() {
        return "hello user";
    }

    @GetMapping("/guest/hello")
    public String guest() {
        return "hello guest";
    }

    // 这里这个路径没有在数据库中配置  如果希望没有指定权限的路径公开访问  可以在config中 .app()这个方法中将  object.setRejectPublicInvocations(false);  false改成true
    // 配置文件中对于没有指定权限的路径  全部默认需要认证登录   如果需要放开这个  可以添加匹配规则， .antMatchers("/hello").permitAll()
    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

}
