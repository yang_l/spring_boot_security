package com.example.spring_boot_security.smslogin;

import com.example.spring_boot_security.service.SmsUserService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author YangLi
 * @Date 2024/9/12 17:03
 * @注释
 */

/**
 * 这里是重点   如果有自定义的登录不局限于手机号+验证码的方式，比如指纹，二维码，3方系统认证等都可以通过实现 AuthenticationProvider
 * 然后重写   authenticate() 这个方法实现
 * 步骤就是   实现这个AuthenticationProvider接口，然后重写authenticate()方法，然后在这个方法中验证是否通过
 * 这里也是重点  验证通过后 需要将权限信息写入security中  通过实现 UserDetailsService 这个接口 然后重写里面的 loadUserByUsername(String xx)方法
 * 这里String xx  就是数据库查询的字段   举例
 * 比如你是账号密码登录  这里就是传入账号  然后查询此账号相关的权限信息
 * 如果这里是手机号+验证码的方式通过  需要传入的就是 手机号  然后根据手机号查询相关权限信息并放入security中
 * 这里也有一个点需要注意，就是多种验证方式  可能需要多个接口去是实现UserDetailsService  不同的实现类根据不同的字段去获取权限
 * <p>
 * 最后通过重写 supports（）这个方法告诉security 这个provider 只处理 SmsAuthenticationToken 类型的请求
 * <p>
 * <p>
 * 最后做一个总结：要实现手机号+短信的登录方式，首先需要能发短信 如何发短信请看 https://blog.csdn.net/m0_71666771/article/details/142177586
 * 然后是需要构建一个 实现类去继承 AbstractAuthenticationToken 这个类 比如 叫 xxAuthenticationToken 然后将我们需要的 手机号和验证码字段加上 因为后面security会拦截继承了AbstractAuthenticationToken
 * 的类，通过专门的 provider来处理其中的信息 那具体使用哪个provider呢? 这个需要我们自己自定义一个Provider 比如叫 xxProvider 通过实现 AuthenticationProvider 接口
 * 然后重写 supports（）方法来告诉security 这个 xxAuthenticationToken  由xxProvider 来专门处理，且这个xxProvider 只处理 xxAuthenticationToken
 * 那如何处理呢 ？ 这里我们通过重写 authenticate（）方法来实现  里面写你具体的验证逻辑 验证通过之后  还需要将权限告诉给security 需要一个实现类实现UserDetailsService接口 然后重写里面的 loadUserByUsername(String xx)方法
 * 根据具体字段写入权限  以上就是完整逻辑  其他的也一样比如什么二维码，指纹什么的  万变不离其中， 如果是账号密码的话  security 默认实现了的  不需要重新写token 和provider 只需要把权限写入
 * <p>
 * ok 到这里基本就结束了  但是还需要拦截 你的登录请求 比如 /sms-login 并在这个这个拦截其中获取所需的参数 放入xxAuthenticationToken 然后终止过滤器链 最后吧过滤器或者拦截器在security中注册好就行了
 */
@Component
public class SmsAuthenticationProvider implements AuthenticationProvider {

    @Resource
    private SmsUserService smsUserService;

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String phoneNumber = (String) authentication.getPrincipal();
        String code = (String) authentication.getCredentials();
        // 验证验证码是否正确
        String redisCode = redisTemplate.opsForValue().get("SMS_" + phoneNumber);

        if (!code.equals(redisCode)) {
            throw new AuthenticationException("验证码错误") {
            };
        }
        // 验证通过后，加载用户详情（例如权限信息）
        UserDetails userDetails = smsUserService.loadUserByUsername(phoneNumber);
        // 创建认证后的 Token
        return new SmsAuthenticationToken(userDetails, code, userDetails.getAuthorities());
    }

    /**
     * supports() 方法告诉 Spring Security，这个 Provider 只支持处理 SmsAuthenticationToken 类型的认证请求
     *
     * @param authentication ·
     * @return ·
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return SmsAuthenticationToken.class.isAssignableFrom(authentication);
    }
}

