package com.example.spring_boot_security.smslogin;


import lombok.EqualsAndHashCode;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @Author YangLi
 * @Date 2024/9/12 17:02
 * @注释
 */
@EqualsAndHashCode(callSuper = false)
public class SmsAuthenticationToken extends AbstractAuthenticationToken {

    // 为什么要使用final？ 它保证了字段值的不可变性，一旦被创建那么生成的这一个对象的属性值就不会再改变
    // 对于票据等... 需要保证对象值在整个生命周期内都不可变 这不是说生成的每个对象的属性值都一样 而是说每个对象生成后就不可在变

    // 这里还出现了一个问题  序列化的问题，Object本身并没有实现序列化接口， 所以他可以接受所有的对象 即使对象没有实现序列化接口也可以传递
    // 后面可能会出现无法序列化的问题  所以下面字段会出现黄色警告 可以将 Object 类换成 Serializable 类型 这样就只能传入可以序列化的对象
    // 但是这样可传入的对象就必须支持序列化  所以这里我还是选择使用 Object 后面如果出现需要序列化且无法序列化的情况再解决

    // 存储电话号码
    private final Object principal;
    // 存储验证码
    private final Object credentials;

    public SmsAuthenticationToken(Object principal, Object credentials) {
        super(null);
        this.principal = principal;
        this.credentials = credentials;
        setAuthenticated(false);
    }

    public SmsAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal;
        this.credentials = credentials;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return this.credentials;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }
}
